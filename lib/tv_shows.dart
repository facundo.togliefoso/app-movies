import 'package:flutter/material.dart';
import "package:movie_app/common/HttpHandler.dart";
import "package:movie_app/model/Media.dart";
import 'package:movie_app/media_list_item.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:movie_app/movie_detail.dart';
import 'package:movie_app/home.dart';
import 'dart:io';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class Tv extends StatefulWidget {
  @override
  _TvState createState() => new _TvState();
}

class _TvState extends State<Tv> {
  List<Media> _media = new List();
  String query;
  List<double> alturas = List();

  int _pageNumber = 1;

  @override
  void initState() {
    super.initState();
    loadShows();
  }

  void loadSearch(String _query) async {
    var search = await HttpHandler().fetchSearchTvShow(_query);
    setState(() {
      _media.addAll(search);
    });
    alturas = [];
    for (int i = 0; i < _media.length; i++) {
      if (_media[i].voteAverage <= 7.0) {
        alturas.add(350.0);
      } else {
        alturas.add(170.0);
      }
    }
  }

  void loadShows() async {
    var series = await HttpHandler().fetchShows(_pageNumber);
    setState(() {
      _media.addAll(series);
    });
    alturas = [];
    for (int i = 0; i < _media.length; i++) {
      if (_media[i].voteAverage <= 7.0) {
        alturas.add(350.0);
      } else {
        alturas.add(170.0);
      }
    }
  }

  Widget miTitulo = Text("TvTruch");
  Icon miIcon = Icon(Icons.search);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(child: DrawerContent()),
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: 200.0,
              floating: false,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                centerTitle: false,
                title: miTitulo,
                background: CachedNetworkImage(
                  imageUrl:
                      "https://assets.pcmag.com/media/images/577155-10-best-original-netflix-tv-series.jpg?thumb=y&width=412&height=412&boxFit=y",
                  placeholder: Container(
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  ),
                ),
              ),
              actions: <Widget>[
                IconButton(
                  icon: miIcon,
                  onPressed: () {
                    setState(
                      () {
                        if (this.miIcon.icon == Icons.search) {
                          this.miIcon = Icon(Icons.close);
                          this.miTitulo = TextField(
                            autofocus: true,
                            style: TextStyle(color: Colors.white),
                            decoration: InputDecoration(
                                prefixIcon:
                                    Icon(Icons.search, color: Colors.white),
                                hintText: "Buscar...",
                                hintStyle: TextStyle(color: Colors.white)),
                            onChanged: (String value) {
                              setState(() {
                                query = value;
                              });
                              if (value == "") {
                                setState(() {
                                  _media = [];
                                });
                                loadShows();
                              } else {
                                setState(() {
                                  _media = [];
                                  _pageNumber = 1;
                                });
                                loadSearch(value);
                              }
                            },
                          );
                        } else {
                          this.miIcon = Icon(Icons.search);
                          this.miTitulo = Text("TvTruch");
                          setState(() {
                            _media = [];
                            _pageNumber = 1;
                          });
                          loadShows();
                        }
                      },
                    );
                  },
                )
              ],
            )
          ];
        },
        body: Container(
          child: _media.length > 0
              ? StaggeredGridView.countBuilder(
                  // scrollDirection: Axis.vertical,
                  padding: const EdgeInsets.only(top: 0.0),
                  itemCount: _media.length,
                  crossAxisCount: 2,
                  itemBuilder: (BuildContext context, int index) {
                    if (index >= _media.length - 1) {
                      _pageNumber++;
                      if (query == "" || query == null) {
                        loadShows();
                      }
                    }
                    return GestureDetector(
                      child: MediaListItem(_media[index], alturas[index]),
                      onTap: () {
                        var route = MaterialPageRoute(
                            builder: (BuildContext context) =>
                                MovieDetail(_media[index]));
                        Navigator.of(context).push(route);
                      },
                    );
                  },
                  staggeredTileBuilder: (int index) => StaggeredTile.count(
                      1, _media[index].voteAverage <= 7.0 ? 2 : 1),
                  crossAxisSpacing: 4,
                  mainAxisSpacing: 4,
                )
              : EmptyBody(),
        ),
      ),
    );
  }
}

class DrawerContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        UserAccountsDrawerHeader(
          accountName: null,
          accountEmail: null,
          decoration: BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.fill,
              image: NetworkImage(
                  "https://cdn.mos.cms.futurecdn.net/Rg8zUgWJbg7BKqTZDkjb8A-320-80.jpg"),
            ),
          ),
        ),
        ListTile(
          title: Text("Películas"),
          trailing: Icon(Icons.local_movies),
          onTap: () {
            var route =
                MaterialPageRoute(builder: (BuildContext context) => Home());
            Navigator.of(context).push(route);
          },
        ),
        Divider(
          height: 5.0,
        ),
        ListTile(
          selected: true,
          title: Text("Televisión"),
          trailing: Icon(Icons.live_tv),
          onTap: () {},
        ),
        Divider(
          height: 5.0,
        ),
        AboutListTile(
          child: Text("Información de la App"),
          applicationIcon: Icon(
            Icons.favorite,
            color: Colors.red,
          ),
          icon: Icon(Icons.info),
          applicationVersion: "v Alpha 1.0.0",
          applicationName: "Tvs",
        ),
        Divider(
          height: 5,
        ),
        Divider(
          height: 205.0,
        ),
        ListTile(
          title: Text("Cerrar"),
          trailing: Icon(Icons.close),
          onTap: () => exit(0),
        ),
      ],
    );
  }
}

class EmptyBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          CircularProgressIndicator(),
        ],
      ),
    );
  }
}
