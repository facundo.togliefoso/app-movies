import 'package:flutter/material.dart';
import 'package:movie_app/model/Media.dart';
import 'package:cached_network_image/cached_network_image.dart';

class MediaListItem extends StatelessWidget {
  final Media media;
  final double altura;

  MediaListItem(this.media, this.altura);

  dynamic _isThereImageBackDrop() {
    if (media.backdropPath == null || media.backdropPath == "") {
      return FadeInImage.assetNetwork(
        placeholder: "lib/assets/foto.jpg",
        image:
            "http://www.virtualblueridge.com/news-and-events/images/20111207c.jpg",
        fit: BoxFit.fill,
        fadeInDuration: new Duration(milliseconds: 40),
      );
    } else {
      return FadeInImage.assetNetwork(
        placeholder: "lib/assets/foto.jpg",
        image: media.getBackDropUrl(),
        fit: BoxFit.fill,
        fadeInDuration: new Duration(milliseconds: 40),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(3),
      child: Container(
        decoration: ShapeDecoration(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            image: DecorationImage(
              fit: BoxFit.cover,
              image: CachedNetworkImageProvider(media.getBackDropUrl()),
            )),
        child: Stack(
          children: <Widget>[
            Positioned(
              left: 0.0,
              bottom: 0.0,
              right: 0.0,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(15),
                    bottomRight: Radius.circular(15),
                  ),
                  color: Colors.grey[900]
                      .withOpacity(0.5), //le doy opacidad en este caso del 50%
                ),
                constraints: new BoxConstraints.expand(
                  //restriccion al elemento en este caso solo le dejo 55pixeles
                  height: 55.0,
                ),
              ),
            ),
            Positioned(
              left: 10.0,
              bottom: 10.0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    width: 120.0,
                    child: Text(
                      media.title,
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.white),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Container(
                    width: 120.0,
                    padding: const EdgeInsets.only(top: 4.0),
                    child: Text(
                      media.getGenres() != null ? media.getGenres() : "",
                      style: TextStyle(color: Colors.white),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  )
                ],
              ),
            ),
            new Positioned(
              right: 5.0,
              bottom: 10.0,
              child: new Column(
                children: <Widget>[
                  new Row(children: <Widget>[
                    new Text(media.voteAverage.toString()),
                    new Container(
                      width: 4.0,
                    ),
                    new Icon(
                      Icons.star,
                      color: Colors.white,
                      size: 16.0,
                    ),
                  ]),
                  new Container(
                    height: 4.0,
                  ),
                  new Row(
                    children: <Widget>[
                      new Text(media.getReleaseYear().toString()),
                      new Container(
                        width: 4.0,
                      ),
                      new Icon(Icons.date_range,
                          color: Colors.white, size: 16.0),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
