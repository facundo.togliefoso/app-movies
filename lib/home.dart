import 'package:flutter/material.dart';
import "package:movie_app/common/HttpHandler.dart";
import "package:movie_app/model/Media.dart";
import 'media_list_item.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'movie_detail.dart';
import 'tv_shows.dart';
import 'dart:io';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => new _HomeState();
}

class _HomeState extends State<Home> with TickerProviderStateMixin {
  Widget miTitulo = Text("MovieTruch");
  Icon miIcon = Icon(Icons.search);
  List<Media> _media = new List();
  String query;

  int _pageNumber = 1;

  @override
  void initState() {
    super.initState();
    loadMovies();
  }

  void loadSearch(String _query) async {
    var search = await HttpHandler().fetchSearch(_query);
    setState(() {
      _media.addAll(search);
    });
  }

  void loadMovies() async {
    var movies = await HttpHandler().fetchMovies(_pageNumber);
    setState(() {
      _media.addAll(movies);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(child: DrawerContent()),
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: 200.0,
              floating: false,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                centerTitle: false,
                title: miTitulo,
                background: CachedNetworkImage(
                  imageUrl:
                      "https://moviestvnetwork.com/images/Movies-Schedule.jpeg",
                  placeholder: Container(
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  ),
                  errorWidget: Icon(Icons.error),
                ),
              ),
              actions: <Widget>[
                IconButton(
                  icon: miIcon,
                  onPressed: () {
                    setState(
                      () {
                        if (this.miIcon.icon == Icons.search) {
                          this.miIcon = Icon(Icons.close);
                          this.miTitulo = TextField(
                            autofocus: true,
                            style: TextStyle(color: Colors.white),
                            decoration: InputDecoration(
                                prefixIcon:
                                    Icon(Icons.search, color: Colors.white),
                                hintText: "Buscar...",
                                hintStyle: TextStyle(color: Colors.white)),
                            onChanged: (String value) {
                              setState(() {
                                query = value;
                              });
                              if (value == "") {
                                setState(() {
                                  _media = [];
                                });
                                loadMovies();
                              } else {
                                setState(() {
                                  _media = [];
                                  _pageNumber = 1;
                                });
                                loadSearch(value);
                              }
                            },
                          );
                        } else {
                          this.miIcon = Icon(Icons.search);
                          this.miTitulo = Text("MovieTruch");
                          setState(() {
                            _media = [];
                            _pageNumber = 1;
                          });
                          loadMovies();
                        }
                      },
                    );
                  },
                )
              ],
            )
          ];
        },
        body: Container(
          child: _media.length > 0
              ? GridView.builder(
                  padding: const EdgeInsets.only(top: 0.0),
                  itemCount: _media.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2),
                  itemBuilder: (BuildContext context, int index) {
                    if (index >= _media.length - 1) {
                      _pageNumber++;
                      if (query == "" || query == null) {
                        loadMovies();
                      }
                    }
                    return GestureDetector(
                      child: MediaListItem(_media[index], 172),
                      onTap: () {
                        var route = MaterialPageRoute(
                            builder: (BuildContext context) =>
                                MovieDetail(_media[index]));
                        Navigator.of(context).push(route);
                      },
                    );
                  })
              : EmptyBody(),
        ),
      ),
    );
  }
}

class DrawerContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        UserAccountsDrawerHeader(
          accountName: null,
          accountEmail: null,
          decoration: BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.fill,
              image: NetworkImage(
                  "https://res.cloudinary.com/people-matters/image/upload/q_auto,f_auto/v1517845732/1517845731.jpg"),
            ),
          ),
        ),
        ListTile(
          selected: true,
          title: Text("Películas"),
          trailing: Icon(Icons.local_movies),
          onTap: () {},
        ),
        Divider(
          height: 5.0,
        ),
        ListTile(
          title: Text("Televisión"),
          trailing: Icon(Icons.live_tv),
          onTap: () {
            var route =
                MaterialPageRoute(builder: (BuildContext context) => Tv());
            Navigator.of(context).push(route);
          },
        ),
        Divider(
          height: 5.0,
        ),
        AboutListTile(
          child: Text("Información de la App"),
          applicationIcon: Icon(
            Icons.favorite,
            color: Colors.red,
          ),
          icon: Icon(Icons.info),
          applicationVersion: "v Alpha 1.0.0",
          applicationName: "Movies",
        ),
        Divider(
          height: 5,
        ),
        Divider(
          height: 205.0,
        ),
        ListTile(
          title: Text("Cerrar"),
          trailing: Icon(Icons.close),
          onTap: () => exit(0),
        ),
      ],
    );
  }
}

class EmptyBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          CircularProgressIndicator(),
        ],
      ),
    );
  }
}
