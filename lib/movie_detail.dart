import 'model/Media.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:url_launcher/url_launcher.dart';
import "package:movie_app/model/Media.dart";
import "package:movie_app/common/HttpHandler.dart";

class MovieDetail extends StatefulWidget {
  final Media myMedia;

  MovieDetail(this.myMedia);

  @override
  _MovieDetailState createState() => new _MovieDetailState();
}

class _MovieDetailState extends State<MovieDetail> {
  @override
  void initState() {
    super.initState();
    loadDetails();
  }

  void loadDetails() async {
    var movies = await HttpHandler().detailMovie(widget.myMedia.id);
    setState(() {
      widget.myMedia.homepage = movies.homepage;
      print(widget.myMedia.homepage);
    });
  }


  _launchURL() async {
    var url = widget.myMedia.homepage;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw "Could not launch $url";
    }
  }

  dynamic _isThereOverview() {
    if (widget.myMedia.overview == null || widget.myMedia.overview == "") {
      return Text(
        "No hay resumen de este elemento.",
        style: TextStyle(color: Colors.red, fontSize: 24),
      );
    } else {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Resumen: ",
            style: TextStyle(fontSize: 16),
          ),
          Container(
            padding: const EdgeInsets.only(top: 20),
          ),
          Card(
            child: Container(
              padding: const EdgeInsets.all(20),
              child: Text(
                widget.myMedia.overview,
                softWrap: true,
                style: TextStyle(fontSize: 16),
              ),
            ),
          )
        ],
      );
    }
  }

  dynamic _isThereImageBackDrop() {
    if (widget.myMedia.backdropPath == null ||
        widget.myMedia.backdropPath == "") {
      return FadeInImage.assetNetwork(
        placeholder: "lib/assets/foto.jpg",
        image:
            "https://senseeventsl.files.wordpress.com/2018/02/not-found-logo.png",
      );
    } else {
      return FadeInImage.assetNetwork(
        placeholder: "lib/assets/foto.jpg",
        image: widget.myMedia.getBackDropUrl(),
      );
    }
  }

  dynamic _isThereImagePosterDrop() {
    if (widget.myMedia.posterPath == null || widget.myMedia.posterPath == "") {
      return FadeInImage.assetNetwork(
        placeholder: "lib/assets/foto.jpg",
        image:
            "http://www.virtualblueridge.com/news-and-events/images/20111207c.jpg",
        fit: BoxFit.fill,
        fadeInDuration: Duration(milliseconds: 40),
      );
    } else {
      return FadeInImage.assetNetwork(
        placeholder: "lib/assets/foto.jpg",
        image: widget.myMedia.getPosterUrl(),
        width: 150.0,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: ListView(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 2),
              ),
              ClipPath(
                clipper: ClippingClass(),
                child: _isThereImageBackDrop(),
              ),
              IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          ),
          Container(
            padding: const EdgeInsets.all(32.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: _isThereImagePosterDrop(),
                        margin: EdgeInsets.only(right: 20.0),
                      )
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.only(bottom: 8.0),
                        child: Text(
                          "Title: " + widget.myMedia.title,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20),
                        ),
                      ),
                      Text(
                        "Release Date: " + widget.myMedia.releaseDate,
                        style: TextStyle(
                          color: Colors.grey[500],
                        ),
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.star,
                              color: Colors.red[500],
                            ),
                            Text('${widget.myMedia.voteAverage}'),
                          ],
                        ),
                      ),
                      RaisedButton(
                        onPressed: () {
                          if (widget.myMedia.homepage == "" ||
                              widget.myMedia.homepage == null) {
                            AlertDialog dialog = AlertDialog(
                              title: Text("En: ${widget.myMedia.title}"),
                              content: Text("No hay homepage"),
                            );
                            showDialog(context: context, child: dialog);
                          } else {
                            _launchURL();
                          }
                        },
                        child: Text("Homepage"),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            decoration: BoxDecoration(border: Border.all(color: Colors.white)),
            padding: const EdgeInsets.all(10.0),
            child: _isThereOverview(),
          )
        ],
      ),
    );
  }
}

class ClippingClass extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0.0, size.height - 40);
    path.quadraticBezierTo(
        size.width / 4, size.height, size.width / 2, size.height);
    path.quadraticBezierTo(size.width - (size.width / 4), size.height,
        size.width, size.height - 40);
    path.lineTo(size.width, 0.0);
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
