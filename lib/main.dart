import 'package:flutter/material.dart';
import "package:movie_app/home.dart";

void main() {
  runApp(new MaterialApp(
    debugShowCheckedModeBanner: false,
   home: Home(),
   theme: new ThemeData.dark(),
  ));
}