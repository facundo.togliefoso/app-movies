import "dart:async";
import "package:http/http.dart" as http;
import 'dart:convert';
import "package:movie_app/common/Constants.dart";
import "package:movie_app/model/Media.dart";

class HttpHandler {
  static final _httpHandler = new HttpHandler();
  final String _baseUrl = "api.themoviedb.org";
  final String _language = "es-ES";
  final String _query = "";

  static HttpHandler get() {
    return _httpHandler;
  }

  Future<dynamic> getJson(Uri uri) async {
    http.Response response = await http.get(uri);
    return json.decode(response.body);
  }

  Future<List<Media>> fetchMovies(int _pageNumber) {
    //This is to get the popular movies
    var uri = new Uri.https(_baseUrl, "3/movie/popular", {
      "api_key": API_KEY,
      "language": _language,
      "page": _pageNumber.toString()
    });

    return getJson(uri).then(
      (data) => data["results"]
          .map<Media>((item) => new Media(item, MediaType.movie))
          .toList(),
    );
  }

  Future<List<Media>> fetchShows(int _pageNumber) {
    var uri = new Uri.https(_baseUrl, "3/tv/popular", {
      "api_key": API_KEY,
      "language": _language,
      "page": _pageNumber.toString()
    });

    print(_pageNumber);
    return getJson(uri).then(
      (data) => data["results"]
          .map<Media>((item) => new Media(item, MediaType.show))
          .toList(),
    );
  }

  Future<List<Media>> fetchSearchTvShow(String _query) {
    var uri = new Uri.https(_baseUrl, "3/search/tv", {
      "api_key": API_KEY,
      "language": _language,
      "query": _query,
      "page": "1",
    });

    return getJson(uri).then(
      (data) => data["results"]
          .map<Media>((item) => new Media(item, MediaType.show))
          .toList(),
    );
  }

  Future<List<Media>> fetchSearch(String _query) {
    //This one is to search any movie
    var uri = new Uri.https(_baseUrl, "3/search/movie", {
      "api_key": API_KEY,
      "language": _language,
      "query": _query,
      "page": "1",
    });

    return getJson(uri).then(
      (data) => data["results"]
          .map<Media>((item) => new Media(item, MediaType.movie))
          .toList(),
    );
  }

  Future<Media> detailMovie(int id) {
    var uri = new Uri.https(_baseUrl, "3/movie/{id}", {
      "api_key": API_KEY,
      "language": _language,
    });

    var media = getJson(uri).then((data) => Media(data, MediaType.movie));

    return media;
  }


  Future<Media> detailSerie(int id) {
    var uri = new Uri.https(_baseUrl, "3/tv/{id}", {
      "api_key": API_KEY,
      "language": _language,
    });

    var media = getJson(uri).then((data) => Media(data, MediaType.movie));

    return media;
  }
}
